emu8051 (2.0.1-2) unstable; urgency=medium

  * Switch to debhelper 13
  * Do not link with -Wl,--as-needed, it is now the default
  * Fix FTBFS with GCC 10 (Closes: #957174)
  * Bump Standards-Version to 4.5.0, no changes

 -- Graham Inggs <ginggs@debian.org>  Tue, 14 Jul 2020 09:14:34 +0000

emu8051 (2.0.1-1) unstable; urgency=medium

  * Adopt package on behalf of Debian Electronics Team (Closes: #846812)
  * Update upstream home page
  * Update debian/watch
  * New upstream release (Closes: #932352)
  * Switch to debhelper 12
  * Set Rules-Requires-Root: no
  * Simplify debian/rules
  * Update debian/copyright
  * Drop debian/menu, see #741573
  * Bump Standards-Version to 4.4.0, no changes

 -- Graham Inggs <ginggs@debian.org>  Tue, 13 Aug 2019 13:55:09 +0000

emu8051 (1.1.1-1) unstable; urgency=low

  * New Upstream release.
  * debian/control:
    + Update standards to 3.9.5
  * debian/copyright:
    + Update to latest standards
    + Packaging is now under GPL v2
  * debian/rules:
    + Link the executables to emu 8051 man page
    + Enable all hardening flags
  * debian/watch:
    + Incorporate latest changes. Thanks Bart!
  * Explicitly link emu8051-gtk in menu and desktop file as the
    executables have split.
  * Add keywords string in .desktop file.

 -- Bhavani Shankar <bhavi@ubuntu.com>  Sun, 29 Dec 2013 13:37:28 +0530

emu8051 (1.1.0-1) unstable; urgency=low

  * Initial Release. Closes: #585446
    + Thanks to Pierre Ferrari <pierre.ferrari@cpln.ch> for initial
      packaging in ubuntu.
  * New upstream release with packaging improvements:
  * debian/control:
    + Switch over to debhelper v7 tiny instead of cdbs and as a result
      add Build-Depend on autotools-dev
    + Bump up standards-version to 3.8.4. No other changes
    + Package is now in electronics section
  * debian/rules:
    + Minimize rules file and add -Wl,-z,defs -Wl,--as-needed to GTK
      LDFLAGS
  * debian/compat:
    + Set to version 7
  * debian/manpages:
    + (New file). Install emu8051 manpage
  * debian/docs:
    + (New file). Install TODO and NEWS files
  * Switch to 3.0 (quilt) format
  * Update debian/copyright layout
  * Add a desktop and menu file

 -- Bhavani Shankar <bhavi@ubuntu.com>  Sun, 13 Jun 2010 11:25:29 +0530

emu8051 (1.0.2-0ubuntu1) jaunty; urgency=low

  * new upstream version (1.0.2)
  * add copyright and changing the FSF address in some files
  * write a new long description

 -- Pierre Ferrari <pierre.ferrari@cpln.ch>  Tue, 17 Feb 2009 08:20:14 +0100
